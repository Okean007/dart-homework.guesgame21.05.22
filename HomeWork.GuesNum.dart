import 'dart:io';

import 'dart:math';

void main(List<String> args) {
  game();
}

game() {
  print("Добро пожаловать в игру 'Отгадай число'!");
  print("\nПравила игры загадать число от 1 до 100");
  print("Используйте  \"Больше\", \"Меньше\", \"Да\"");
  int stepComputer = 0;
  int stepIgrok = 0;
  bool flag = false;

  while (flag == false) {
    print('Выбрите режим игры кто отгадывает?');
    print(
        "1-Отгадывает компьютер \n2-Отгадыввает пользователь \n3-Режим дуэль\n4-выйти");

    String? vybor = stdin.readLineSync();
    if (vybor == '1') {
      stepComputer = guesUser();
    } else if (vybor == '2') {
      stepIgrok = guesComputer();
    } else if (vybor == '3') {
      print('Отгадывает компьютер');
      stepComputer = guesUser();
      print('Отгадывает Пользователь');
      stepIgrok = guesComputer();
      print(
          '${resultGame(stepComputer, stepIgrok)}\nКомпьютер: $stepComputer\nПользователь: $stepIgrok');
      break;
    } else if (vybor == '4') {
      flag = true;
    }
  }
 }

//Переменная функция результат игры
resultGame(int stepComputer, int stepUser) {
  stepComputer == stepUser;
  String win = '';

  if (stepUser < stepComputer) {
    win = 'Дуэль: Выиграл игрок';
  } else if (stepUser > stepComputer) {
    win = 'Дуэль: выиграл компьютер';
  } else {
    win = 'Ничья';
  }
  return win;
}

//Переменная функция пользователь загадывает число
guesUser() {
  print("Я постараюсь отгадать его за минимальное число попыток(КОМПЬЮТЕР).");
  bool flag = false;
  int step = 0;
  double min = 1;
  double max = 100;
  while (flag == false) {
    step += 1;
    double answerComp = min + (max - min) / 2;
    print(answerComp.floor());
    String answer = stdin.readLineSync()!;
    if (answer == 'yes') {
      print('Компьютер угадал за $step попыток');
      return step;
    } else if (answer == 'less') {
      max = answerComp;
    } else if (answer == 'greater') {
      min = answerComp;
    }
  }
}

//переменная функция компьютер загадывает число
guesComputer() {
  bool flag = false;
  int stepUser = 0;
  int gues;
  Random rand = new Random();
  int answer = rand.nextInt(100);
  print('Ваш ответ(Пользователь):');
  while (flag == false) {
    stepUser += 1;
    String? command = (stdin.readLineSync()!);
    gues = int.parse(command);
    if (gues < answer) {
      print('greater');
    } else if (gues > answer) {
      print('less');
    } else if (gues == answer) {
      print('Да! Пользователь угадал за $stepUser шагов');
      flag = true;
      return stepUser;
    }
  }
}

